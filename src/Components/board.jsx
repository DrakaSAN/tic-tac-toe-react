import React from 'react';
import PropTypes from 'prop-types';
import Square from './square.jsx';
import { Squares } from '../core/types';
import { gameSize } from '../core/properties';

export default function Board (props) {
  return (
    <div>
      {Array.from({ length: gameSize }).map((_, i) => {
        const x = i * gameSize;
        return (
          <div className="board-row" key={`row-${x}`}>
            {Array.from({ length: gameSize }).map((_, j) => {
              const y = x + j;
              return (
                <Square
                  id={y}
                  value={props.squares[y]}
                  onClick={() => props.onClick(y)}
                  winning={props.winningPosition?.includes(y)}
                  key={`square-${y}`}
                />
              );
            })}
          </div>
        );
      })}
    </div>
  );
}

Board.propTypes = {
  squares: Squares.isRequired,
  onClick: PropTypes.func.isRequired,
  winningPosition: PropTypes.arrayOf(PropTypes.number)
};

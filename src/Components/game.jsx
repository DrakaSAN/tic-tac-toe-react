import React, { useReducer } from 'react';
import Board from './board.jsx';
import Status from './status.jsx';
import History from './history.jsx';
import { getCoordinates } from '../core/square';
import { calculateWinner } from '../core/winner';

/**
 * @typedef {import("../core/types").State} State
 * @typedef {import("../core/types").Turn} Turn
 * @typedef {import("../core/types").Action} Action
 */

/**
 * @callback Dispatch
 * @param {Action} action
 */

export const /** @type {State} */ initialState = {
  history: [{
    squares: Array(9).fill(undefined),
    currentTurn: 'X',
    winner: undefined,
    step: 0
  }],
  reverseOrder: false
};

/**
 * 
 * @param {State} state 
 * @param {number} [turn] - Defaults to the current turn
 * @returns {Turn}
 */
export function getTurn(state, turn = state.history.length - 1) {
  const current = state.history[turn];
  return {
    ...current,
    squares: current.squares.slice(),
  };
}

/**
 * @param {State} state 
 * @returns {State}
 */
export function reverseOrder(state) {
  return {
    ...state,
    reverseOrder: !state.reverseOrder
  };
}

/**
 * 
 * @param {State} state 
 * @param {number} turn 
 * @returns {State}
 */
export function jumpTo(state, turn) {
  return {
    ...state,
    history: state.history.slice(0, turn + 1)
  };
}

/**
 * 
 * @param {State} state
 * @param {number} squareId
 * @returns {State}
 */
export function handlePlayerTurn(state, squareId) {
  const { squares, winner, currentTurn, step } = getTurn(state);
  if (winner || squares[squareId]) {
    return state;
  }
  squares[squareId] = currentTurn;

  const /** @type {Turn} */ turn = {
    squares,
    currentTurn: (currentTurn === 'X') ? 'O' : 'X',
    winner: calculateWinner(squares),
    step: step + 1,
    coordinates: getCoordinates(squareId)
  };

  return {
    ...state,
    history: state.history.concat(turn)
  };
}

/**
 * @param {State} state
 * @param {Action} action
 * @returns {State}
 */
export function reducerHandler(state, action) {
  if (action.type === 'reverseOrder') {
    return reverseOrder(state);
  }

  if (action.type === 'jumpTo') {
    if (!action.turn) {
      throw new Error(`jumpTo needs a turn number (${JSON.stringify(action)})`);
    }
    return jumpTo(state, action.turn);
  }

  if (action.type === 'handlePlayerTurn') {
    if (!action.squareId && action.squareId !== 0) {
      throw new Error(`handlePlayerTurn needs a squareId (${JSON.stringify(action)})`);
    }	
    return handlePlayerTurn(state, action.squareId);
  }

  throw new Error(`Unknown reducer action ${action.type} (${JSON.stringify(action)})`);
}

export default function Game () {
  const [ state, dispatch ] = useReducer(reducerHandler, initialState);
  const { squares, winner, currentTurn, step } = getTurn(state);

  return (
    <div className="game">
      <div className="game-board">
        <Board
          squares={squares}
          onClick={(squareId) => dispatch({ type: 'handlePlayerTurn', squareId })}
          winningPosition={winner?.position}
        />
      </div>
      <div className="game-info">
        <Status
          winner={winner?.player}
          currentTurn={currentTurn}
          step={step}
        ></Status>
        <button onClick={() => dispatch({ type: 'reverseOrder' })}>Reverse sort order</button>
        <History
          history={state.history}
          reversed={state.reverseOrder}
          onClick={(turn) => dispatch({ type: 'jumpTo', turn })}
        ></History>
      </div>
    </div>
  );
}

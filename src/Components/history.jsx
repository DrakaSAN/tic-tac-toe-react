import React from 'react';
import PropTypes from 'prop-types';
import { State } from '../core/types';

export default function History(props) {
  const moves = props.history.map((turn, i) => {
    const description = (i === 0) ? '(GAME START)' : `(${(turn.currentTurn === 'X') ? 'O' : 'X'} play on ${turn.coordinates.x} / ${turn.coordinates.y})`;
    const className = (i === props.history.length - 1) ? 'history current-turn' : 'history';

    return (<li key={turn.step}><button onClick={() => props.onClick(i)} className={className}>Go to turn {i} {description}</button></li>);
  });
  if (props.reversed) {
    return <ol>{moves.reverse()}</ol>;
  }
  return <ol>{moves}</ol>;
}

History.propTypes = {
  history: State.history,
  reversed: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired
};
import React from 'react';
import PropTypes from 'prop-types';
import { PlayerValues } from '../core/types';

export default function Square(props) {
  return (
    <button
      className={props.winning ? 'square winning' : 'square'}
      onClick={() => props.onClick()}
    >
      {props.value}
    </button>
  );
}

Square.propTypes = {
  winning: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  value: PropTypes.oneOf([...PlayerValues, undefined])
};

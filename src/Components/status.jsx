import React from 'react';
import PropTypes from 'prop-types';
import { Player, PlayerValues } from '../core/types';

export default function Status(props) {
  if (props.winner) {
    return (<div>Winner: {props.winner}</div>);
  }
  if (props.winner === false) {
    return (<div>Draw</div>);
  }
  return (<div>Current player: {props.currentTurn}. Step {props.step}</div>);
}

Status.propTypes = {
  winner: PropTypes.oneOf([...PlayerValues, false]),
  currentTurn: Player.isRequired,
  step: PropTypes.number.isRequired
};
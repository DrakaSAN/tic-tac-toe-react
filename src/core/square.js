import { gameSize } from './properties';

/** @typedef {import('./types').Coordinates} Coordinates */

/**
 * 
 * @param {number} squareId 
 * @returns {Coordinates}
 */
export function getCoordinates(squareId) {
  return { x: squareId % gameSize, y: Math.floor(squareId / gameSize) };
}

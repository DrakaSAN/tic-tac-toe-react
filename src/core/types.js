import PropTypes from 'prop-types';

/**
 * @typedef { ('X' | 'O') } Player
 */
export const PlayerValues = ['X', 'O'];
export const Player = PropTypes.oneOf(PlayerValues);

/** @typedef {Array<Player | undefined>} Squares */
export const Squares = PropTypes.arrayOf(Player);

/**
 * @typedef {{
* 	player: Player | false
* 	position?: Array<number>
* }} Winner
* player is the winning player, or null in cases of draw
* position is the set of squareIds that were detected as the winning position
*/
export const Winner = {
  player: PropTypes.oneOf([...PlayerValues, false]),
  position: PropTypes.arrayOf(PropTypes.number).isRequired
};

/**
 * @typedef { {
*  x: number
*  y: number
* } } Coordinates
*/
export const Coordinates = {
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired
};

/**
 * @typedef { { 
* 	squares: Squares,
* 	currentTurn: Player,
* 	winner?: Winner,
* 	step: number
* 	coordinates?: Coordinates
* } } Turn
*/
export const Turn = {
  squares: Squares.isRequired,
  currentTurn: Player.isRequired,
  winner: PropTypes.shape(Winner),
  step: PropTypes.number.isRequired,
  coordinates: PropTypes.shape(Coordinates)
};

/**
* @typedef { {
* 	history: Array<Turn>,
* 	reverseOrder: boolean
* } } State
*/
export const State = {
  history: PropTypes.arrayOf(PropTypes.shape(Turn)).isRequired,
  reverseOrder: PropTypes.bool.isRequired
};

/**
* @typedef { {
* 	type: 'reverseOrder' | 'jumpTo' | 'handlePlayerTurn'
* 	turn?: number
* 	squareId?: number
* } } Action
* turn is required for the 'jumpTo' type
* squareId is required for the 'handlePlayerTurn' type
*/
export const Action = {
  type: PropTypes.string.isRequired,
  turn: PropTypes.number,
  squareId: PropTypes.number
};

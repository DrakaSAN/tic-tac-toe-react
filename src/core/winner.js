import { gameSize } from './properties';

/**
 * @typedef {import('../core/types').Squares} Squares
 * @typedef {import('../core/types').Player} Player
 * @typedef {import('../core/types').Winner} Winner
 */

/**
 * @param {number} gameSize 
 * @returns {Array<Array<number>>}
 */
export function generateWinningState(gameSize) {
  const winningState = [];

  // Filled rows
  const row = Array.from({ length: gameSize }).map((_, i) => i);
  for (let i = 0; i < gameSize; i++) {
    winningState.push(row.map(v => v + (i * gameSize)));
  }

  // Filled columns
  const column = Array.from({ length: gameSize }).map((_, i) => i * gameSize);
  for (let i = 0; i < gameSize; i++) {
    winningState.push(column.map(v => v + i));
  }

  // Diagonales
  {
    const diag = [];
    let x = 0;
    for (let i = 0; i < gameSize; i += 1) {
      diag.push(x);
      x += gameSize + 1;
    }
    winningState.push(diag);
  }

  {
    const diag = [];
    let x = gameSize - 1;
    for (let i = 0; i < gameSize; i += 1) {
      diag.push(x);
      x += gameSize - 1;
    }
    winningState.push(diag);
  }

  return winningState;
}

const /** @type {Array<Array<number>>} */ winningStates = generateWinningState(gameSize);

/**
 * @param {Squares} squares 
 * @param {Player} player 
 * @returns {number[] | undefined}
 */
export function getWinningPosition(squares, player) {
  return winningStates.find((winningState) => {
    const currentState = winningState.map(i => squares[i]);
    return currentState.every(v => v === player);
  });
}

/**
 * @param {Squares} squares
 * @returns {Winner | undefined}
 */
export function calculateWinner(squares) {
  const XWinningPosition = getWinningPosition(squares, 'X');
  if (XWinningPosition) {
    return { player: 'X', position: XWinningPosition };
  }
  const OWinningPosition = getWinningPosition(squares, 'O');
  if (OWinningPosition) {
    return { player: 'O', position: OWinningPosition };
  }
	
  if (squares.every(square => !!square)) {
    return { player: false };
  }
  return undefined;
}

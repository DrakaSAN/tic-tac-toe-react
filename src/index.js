import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import Game from './Components/game.jsx';

// ========================================

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // eslint-disable-next-line react/jsx-filename-extension
  <React.StrictMode>
    <Game />
  </React.StrictMode>
);
